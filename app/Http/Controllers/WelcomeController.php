<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WelcomeController extends Controller
{
    public function index()
    {
        $category = Category::all();
        
        return view('frontend.index', compact('category'));
    }
    public function show($id)
    {
        return view('frontend.detail');
    }
}
