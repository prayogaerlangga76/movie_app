<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert;

class LoginController extends Controller
{
    public function index()
    {
        return view('login.index');
    }
    public function registrasi()
    {
        return view('login.registrasi');
    }
    public function storeRegistrasi(Request $request)
    {
        $requestAll = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
            'name' => 'required'
        ]);
        $user = new User;
        $user->name = strip_tags($requestAll['name']);
        $user->password = Hash::make($requestAll['password']);
        $user->email = strip_tags($requestAll['email']);
        $user->save();
        Alert::success('Berhasil', 'Silahkan login menggunakan akun menggunakan akun yang terdaftar');
        return redirect('/login');
    }
    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return redirect()->intended('/dashboard');
        }
        Alert::error('Gagal', 'Akun tidak terdaftar');
        return back()->with('loginError', 'Login Failed!');
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
