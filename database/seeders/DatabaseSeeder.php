<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Customer;
use App\Models\Menu;
use App\Models\Role;
use App\Models\Produk;
use App\Models\Submenu;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        Role::create([
            'name' => 'Root'
        ]);
        Role::create([
            'name' => 'Admin'
        ]);

        Menu::create([
            'name' => 'Root'
        ]);
       
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Dashboard',
            'url' => '/dashboard',
            'icon' => 'far fa-clock',
            'is_active' => 1,
        ]);
        
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Master Menu',
            'url' => '/menu',
            'icon' => 'fas fa-clipboard-list',
            'is_active' => 1,
        ]);
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Submenu Management',
            'url' => '/submenu',
            'icon' => 'fas fa-list',
            'is_active' => 1,
        ]);
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Master Kategori',
            'url' => '/category',
            'icon' => 'fas fa-table',
            'is_active' => 1,
        ]);
        Submenu::create([
            'menu_id' => 1,
            'title' => 'Ke Tampilan Depan',
            'url' => '/',
            'icon' => 'fas fa-arrow-left',
            'is_active' => 1,
        ]);

        Category::create([
            'category_name' => 'Now Playing',
        ]);
        Category::create([
            'category_name' => 'Popular',
        ]);
        Category::create([
            'category_name' => 'Top Rated',
        ]);
        Category::create([
            'category_name' => 'Up Coming',
        ]);

    }
}
