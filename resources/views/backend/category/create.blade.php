@extends('backend.layouts.main')

@section('content')
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb bg-white">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">{{ $title }}</h4>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6">
                <div class="white-box">
                    <div class="d-md-flex mb-3">
                        <h3 class="box-title mb-0">Daftar Kategori</h3>
                    </div>
                    <div class="col-lg-8">
                        <form action="/category" method="post">
                            @csrf
                            <label for="category_name">Nama Kategori</label>
                            <input type="text" class="form-control @error('category_name') is-invalid @enderror" name="category_name" id="category_name">
                            @error('category_name')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                            <button type="submit" class="btn btn-primary mt-3">Tambah</button>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Recent Comments -->
        <!-- ============================================================== -->
        
    </div>
@endsection