<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.98.0">
    <title>Album example · Bootstrap v5.2</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/album/">


    <link href="{{ asset('assets/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .b-example-divider {
            height: 3rem;
            background-color: rgba(0, 0, 0, .1);
            border: solid rgba(0, 0, 0, .15);
            border-width: 1px 0;
            box-shadow: inset 0 .5em 1.5em rgba(0, 0, 0, .1), inset 0 .125em .5em rgba(0, 0, 0, .15);
        }

        .b-example-vr {
            flex-shrink: 0;
            width: 1.5rem;
            height: 100vh;
        }

        .bi {
            vertical-align: -.125em;
            fill: currentColor;
        }

        .nav-scroller {
            position: relative;
            z-index: 2;
            height: 2.75rem;
            overflow-y: hidden;
        }

        .nav-scroller .nav {
            display: flex;
            flex-wrap: nowrap;
            padding-bottom: 1rem;
            margin-top: -1px;
            overflow-x: auto;
            text-align: center;
            white-space: nowrap;
            -webkit-overflow-scrolling: touch;
        }
    </style>


</head>

<body>

    <header>
        <div class="collapse bg-dark" id="navbarHeader">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-7 py-4">
                        <h4 class="text-white"></h4>
                        <p class="text-muted"></p>
                    </div>
                    <div class="col-sm-4 offset-md-1 py-4">
                        @auth
                            <h4 class="text-white">Logout</h4>

                        @endauth
                        @guest
                            <h4 class="text-white">Login Admin</h4>
                        @endguest
                        <ul class="list-unstyled">
                            @auth
                                <li><a href="/logout" class="text-white">Logout</a></li>
                            @endauth
                            @guest
                                <li><a href="/login" class="text-white">Login</a></li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar navbar-dark bg-dark shadow-sm">
            <div class="container">
                <a href="/" class="navbar-brand d-flex align-items-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="none"
                        stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                        aria-hidden="true" class="me-2" viewBox="0 0 24 24">
                        <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />
                        <circle cx="12" cy="13" r="4" />
                    </svg>
                    <strong>Movie App</strong>
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader"
                    aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </div>
    </header>

    <main>

        <section class="py-5 text-center container">
            <div class="row py-lg-5">
                <div class="col-lg-6 col-md-8 mx-auto">
                    <h1 class="fw-light">Movie App Simple</h1>
                    <p class="lead text-muted">Movie app adalah platform untuk melihat film apa yang sedang tayang, akan
                        datang, populer, film terbaik dan lain sebagainya</p>
                </div>
            </div>
        </section>

        <div class="album py-5 bg-light">
            <div class="container">
                <form class="row g-3">
                    <div class="col-auto">
                        <select name="category_id" id="category_id" class="form-control">
                            <option value="" disabled selected>Filter Kategori</option>
                            @foreach ($category as $c)
                                <option value="{{ $c->id }}">{{ $c->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-auto">
                        <button type="button" class="btn btn-primary mb-3" id="search_data">Search</button>
                    </div>
                </form>
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3 movie">
                    

                    </div>
                </div>
            </div>
        </div>

    </main>

    <footer class="text-muted py-5">
        <div class="container">
            <p class="float-end mb-1">
                <a href="#">Back to top</a>
            </p>
        </div>
    </footer>


    <script src="{{ asset('assets/dist/js/bootstrap.bundle.min.js') }}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            fetchdata();
            $(document).on('click', '#search_data', function(e) {
              var search = $('#category_id').val();
                    e.preventDefault();
                    if(search == 1){
                      $.ajax({
                          type: "POST",
                          url: "https://api.themoviedb.org/3/movie/now_playing?api_key=f0364a53cbd06ce2b2c0897a559a8559",
                          success: function(response) {
                            $('div.row.movie').html("");
                            $.each(response.results, function(key, item) {
                          $('div.row.movie').append('\
                          <div class="col">\
                          <div class="card shadow-sm">\
                              <img class="bd-placeholder-img card-img-top" width="100%" height="225"\
                                  src="https://image.tmdb.org/t/p/original'+item.backdrop_path +'" role="img" aria-label="Placeholder: Thumbnail"\
                                  preserveAspectRatio="xMidYMid slice" focusable="false">\
                                  <title>Placeholder</title>\
                              <div class="card-body">\
                                  <a href="/movie/'+item.id+'"><h4 class="card-text">'+item.original_title+'</h4>\
                                  <div class="d-flex justify-content-between align-items-center">\
                                      <div class="btn-group">\
                                          <p class="badge bg-danger">Now Playing</p>\
                                      </div>\
                                  </div>\
                              </div>\
                            </div>\
                          </div>');
                      });
                          }
                      })
                    } else if(search == 2){
                      $.ajax({
                          type: "POST",
                          url: "https://api.themoviedb.org/3/movie/popular?api_key=f0364a53cbd06ce2b2c0897a559a8559",
                          success: function(response) {
                            $('div.row.movie').html("");
                            $.each(response.results, function(key, item) {
                          $('div.row.movie').append('\
                          <div class="col">\
                          <div class="card shadow-sm">\
                              <img class="bd-placeholder-img card-img-top" width="100%" height="225"\
                                  src="https://image.tmdb.org/t/p/original'+item.backdrop_path +'" role="img" aria-label="Placeholder: Thumbnail"\
                                  preserveAspectRatio="xMidYMid slice" focusable="false">\
                                  <title>Placeholder</title>\
                              <div class="card-body">\
                                  <a href="/movie/'+item.id+'"><h4 class="card-text">'+item.original_title+'</h4>\
                                  <div class="d-flex justify-content-between align-items-center">\
                                      <div class="btn-group">\
                                          <p class="badge bg-warning">Popular</p>\
                                      </div>\
                                  </div>\
                              </div>\
                            </div>\
                          </div>');
                      });
                          }
                      })
                    } else if( search == 3) {
                      $.ajax({
                          type: "POST",
                          url: "https://api.themoviedb.org/3/movie/top_rated?api_key=f0364a53cbd06ce2b2c0897a559a8559",
                          success: function(response) {
                            $('div.row.movie').html("");
                            $.each(response.results, function(key, item) {
                          $('div.row.movie').append('\
                          <div class="col">\
                          <div class="card shadow-sm">\
                              <img class="bd-placeholder-img card-img-top" width="100%" height="225"\
                                  src="https://image.tmdb.org/t/p/original'+item.backdrop_path +'" role="img" aria-label="Placeholder: Thumbnail"\
                                  preserveAspectRatio="xMidYMid slice" focusable="false">\
                                  <title>Placeholder</title>\
                              <div class="card-body">\
                                  <a href="/movie/'+item.id+'"><h4 class="card-text">'+item.original_title+'</h4>\
                                  <div class="d-flex justify-content-between align-items-center">\
                                      <div class="btn-group">\
                                          <p class="badge bg-primary">Top Rated</p>\
                                      </div>\
                                  </div>\
                              </div>\
                            </div>\
                          </div>');
                      });
                          }
                      })
                    } else {
                      $.ajax({
                          type: "POST",
                          url: "https://api.themoviedb.org/3/movie/upcoming?api_key=f0364a53cbd06ce2b2c0897a559a8559",
                          success: function(response) {
                            $('div.row.movie').html("");
                            $.each(response.results, function(key, item) {
                          $('div.row.movie').append('\
                          <div class="col">\
                          <div class="card shadow-sm">\
                              <img class="bd-placeholder-img card-img-top" width="100%" height="225"\
                                  src="https://image.tmdb.org/t/p/original'+item.backdrop_path +'" role="img" aria-label="Placeholder: Thumbnail"\
                                  preserveAspectRatio="xMidYMid slice" focusable="false">\
                                  <title>Placeholder</title>\
                              <div class="card-body">\
                                  <a href="/movie/'+item.id+'"><h4 class="card-text">'+item.original_title+'</h4>\
                                  <div class="d-flex justify-content-between align-items-center">\
                                      <div class="btn-group">\
                                          <p class="badge bg-success">Up Coming</p>\
                                      </div>\
                                  </div>\
                              </div>\
                            </div>\
                          </div>');
                      });
                          }
                      })
                    }

                });
            
            function fetchdata() {

                $.ajax({
                    type: "POST",
                    url: "https://api.themoviedb.org/3/movie/now_playing?api_key=f0364a53cbd06ce2b2c0897a559a8559",
                    dataType: "json",
                    success: function(response) {
                      $.each(response.results, function(key, item) {
                            $('div.row.movie').append('\
                            <div class="col">\
                            <div class="card shadow-sm">\
                                <img class="bd-placeholder-img card-img-top" width="100%" height="225"\
                                    src="https://image.tmdb.org/t/p/original'+item.backdrop_path +'" role="img" aria-label="Placeholder: Thumbnail"\
                                    preserveAspectRatio="xMidYMid slice" focusable="false">\
                                    <title>Placeholder</title>\
                                <div class="card-body">\
                                    <a href="/movie/'+item.id+'"><h4 class="card-text">'+item.original_title+'</h4>\
                                    <div class="d-flex justify-content-between align-items-center">\
                                        <div class="btn-group">\
                                            <p class="badge bg-danger">Now Playing</p>\
                                        </div>\
                                    </div>\
                                </div>\
                              </div>\
                            </div>');
                        });
                    }
                })
            }
            $(document).ready(function() {
                $(document).on('click', '#show_data', function(e) {
                    e.preventDefault();
                    $.ajax({
                        type: "POST",
                        url: "https://api.themoviedb.org/3/movie/now_playing?api_key=f0364a53cbd06ce2b2c0897a559a8559",
                        success: function(response) {
                            
                        }
                    })

                });
            });
        });
    </script>
</body>

</html>
